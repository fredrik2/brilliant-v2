import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../layout/material.module';
import { SurveyService } from './survey.service';
import { SurveyComponent } from './survey.component';
import { ResultComponent } from './result/result.component';
import { SurveyQuestionComponent } from './survey-question/survey-question.component';
import { SummeryComponent } from './result/summery/summery.component';
import { QuestionComponent } from './result/question/question.component';
import { ScoreComponent } from './result/score/score.component';
import { ErrorModalComponent } from './error-modal/error-modal.component';

@NgModule({
  declarations: [
    SurveyComponent,
    ResultComponent,
    SurveyQuestionComponent,
    SummeryComponent,
    QuestionComponent,
    ScoreComponent,
    ErrorModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [SurveyService],
})
export class SurveyModule { }
