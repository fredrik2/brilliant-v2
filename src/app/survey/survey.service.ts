import { Injectable } from '@angular/core';
import { Question, QuestionSubmited } from '../models/question.model'
import { SurveyResult, QuestionResult } from '../models/survey-result.model';
import { Questions } from '../constants/questions';
import { Benchmark } from '../constants/benchmark';

@Injectable({
  providedIn: 'root'
})

export class SurveyService {
  private localStorageItem = 'brilliantSurveyResult';

  private questions: Question[] = Questions;
  private benchmark = Benchmark;

  constructor() { }

  getQuestions(): Question[] {
    return this.questions;
  }

  // In production with an actuall API the following methods would be returning Observables.

  saveResult(submitted: QuestionSubmited[]): void {
    localStorage.setItem(this.localStorageItem, JSON.stringify(submitted));
  }

  hasResult(): boolean {
    if (localStorage.getItem(this.localStorageItem) !== null) {
      return true;
    } else {
      return false;
    }
  }

  getResult(): SurveyResult | null {
    const localStorageStr = localStorage.getItem(this.localStorageItem);

    if (localStorageStr !== null) {
      const surveyData = JSON.parse(localStorageStr) as QuestionSubmited[];
      const result: SurveyResult = new SurveyResult;
      let totalUserScore: number = 0;
      let totalBenchScore: number = 0;

      result.questionScores = [];

      surveyData.forEach(q => {
        const benchScoresArr = this.benchmark.map(n => n[q.QuestionKey]);
        const avgBenchScore = benchScoresArr.reduce((a, b) => a + b, 0)/benchScoresArr.length;
        totalUserScore = totalUserScore+parseInt(q.score as any); // Needs to parse int as JSON saves as string.
        totalBenchScore = totalBenchScore+avgBenchScore;

        result.questionScores.push({
          QuestionKey: q.QuestionKey,
          Text: q.Text,
          userScore: q.score,
          benchmarkScore: avgBenchScore,
        });
      });

      result.benchAvg = Math.round(totalBenchScore/this.benchmark.length * 100) / 100; // Benchmark score rounded to two decimals
      result.userAvg = totalUserScore/surveyData.length;

      return result;
    } else {
      return null;
    }
  }

  clearResult(): void {
    localStorage.removeItem(this.localStorageItem);
  }

}
