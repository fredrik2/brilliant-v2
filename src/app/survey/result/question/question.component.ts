import { Component, Input, OnInit } from '@angular/core';
import { QuestionResult } from 'src/app/models/survey-result.model';

@Component({
  selector: 'app-survey-result-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  constructor() { }

  @Input() number: number;
  @Input() question: QuestionResult;

  ngOnInit(): void {
  }

}
