import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-survey-result-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {

  @Input() userScore: number;
  @Input() avgScore: number;
  @Input() extraText: string;

  constructor() { }

  ngOnInit(): void {
  }

}
