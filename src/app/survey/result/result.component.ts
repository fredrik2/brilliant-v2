import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SurveyResult } from '../../models/survey-result.model';
import { SurveyService } from '../survey.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  surveyResult: SurveyResult | null;

  constructor(
    private router: Router,
    private surveyService: SurveyService
  ) { }

  ngOnInit(): void {
    this.surveyResult = this.surveyService.getResult();
  }

  clear() {
    this.surveyService.clearResult();
    this.router.navigate(['/welcome']);
  }
}
