import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-survey-result-summery',
  templateUrl: './summery.component.html',
  styleUrls: ['./summery.component.scss']
})
export class SummeryComponent implements OnInit {

  @Input() userAvg: number;
  @Input() benchAvg: number;

  constructor() { }

  ngOnInit(): void {
  }

}
