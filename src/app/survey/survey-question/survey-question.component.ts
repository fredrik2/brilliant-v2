import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Question, QuestionSubmited } from 'src/app/models/question.model';

@Component({
  selector: 'app-survey-question',
  templateUrl: './survey-question.component.html',
  styleUrls: ['./survey-question.component.scss']
})

export class SurveyQuestionComponent implements OnInit {

  @Input() index: number;
  @Input() question: Question;

  @Output() submittedScore = new EventEmitter<QuestionSubmited>();

  questionForm: FormGroup;
  submitted: boolean = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.questionForm = this.formBuilder.group({
      score: ['', [Validators.required, Validators.min(1), Validators.max(5)]]
    });
  }
  get score() {
    return this.questionForm.get('score');
  }

  onNext(): void {
    this.submitted = true;;
    const submit: QuestionSubmited = {
      ...this.question,
      index: this.index,
      score: this.score.value,
      valid: this.score.valid
    }

    this.submittedScore.emit(submit);
  }
}
