import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-survey-error-modal',
  templateUrl: './error-modal.component.html',
  styleUrls: ['./error-modal.component.scss']
})
export class ErrorModalComponent implements OnInit {

  @Input() totalInvalidQuestions: number;

  constructor() { }

  ngOnInit(): void {
  }

}
