import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SurveyService } from './survey.service';
import { Question, QuestionSubmited } from '../models/question.model';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  questions: Question[] = [];
  submittedQuestions: QuestionSubmited[] = [];
  unanswered: number = 0;

  @ViewChild('errorDialog', { static: true }) errorDialog: TemplateRef<any>;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private surveyService: SurveyService
  ) { }

  ngOnInit(): void {
    this.questions = this.surveyService.getQuestions();
  }

  get hasResult() {
    return this.surveyService.hasResult();
  }

  onSubmittedScore(question): void {
    const numberOfQuestions = this.questions.length;
    this.submittedQuestions[question.index] = question;

    if (question.index+1 === numberOfQuestions) {
      const validAnswers = this.submittedQuestions.filter(q => q.valid).length;

      if (validAnswers === numberOfQuestions) {
        this.surveyService.saveResult(this.submittedQuestions);
        this.router.navigate(['/result']);
      } else {
        this.unanswered = numberOfQuestions-validAnswers;
        this.dialog.open(this.errorDialog);
      }
    }
  }
}
