export const Questions = [
  {"QuestionKey": "Q1", "Text": "Would you recommend this app to friend?" },
  {"QuestionKey": "Q2", "Text": "Do you trust your leadership?" },
  {"QuestionKey": "Q3", "Text": "How much do you like yourself as a colleague?" },
  {"QuestionKey": "Q4", "Text": "Do you have fun in this app?" },
  {"QuestionKey": "Q5", "Text": "How much do you want to submit?" }
]
