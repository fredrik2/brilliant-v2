import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultComponent } from './survey/result/result.component';
import { SurveyComponent } from './survey/survey.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: '',
    children: [
      { path: 'welcome', component: WelcomeComponent },
      { path: 'survey', component: SurveyComponent },
      { path: 'result', component: ResultComponent },
      { path: '**', redirectTo: '/welcome'}
    ]
  },
  {
    path: '**',
    redirectTo: '/welcome'
  }
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
