export class Question {
  QuestionKey: string;
  Text: string;
}

export class QuestionSubmited {
  index: number;
  Text: string;
  QuestionKey: string;
  score: number;
  valid: boolean;
}
