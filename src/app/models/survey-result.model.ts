export class SurveyResult {
  benchAvg: number;
  userAvg: number;
  questionScores: QuestionResult[];
}

export class QuestionResult {
  Text: string;
  QuestionKey: string;
  userScore: number;
  benchmarkScore: number;
}
