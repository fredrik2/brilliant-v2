import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menu = [];

  constructor(
    private menuService: MenuService
  ) { }

  @Output() public sidenavToggle = new EventEmitter();

  ngOnInit(): void {
    this.menu = this.menuService.menu();
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

}
