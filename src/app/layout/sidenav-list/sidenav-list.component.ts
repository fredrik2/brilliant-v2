import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();

  menu = [];

  constructor(private menuService: MenuService ) { }

  ngOnInit(): void {
    this.menu = this.menuService.menu();
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }

}
