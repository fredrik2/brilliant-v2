import { Injectable } from '@angular/core';
import { MenuItem } from '../models/menu-item.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private baseMenu: MenuItem[] = [
    {
      route: '/welcome',
      caption: 'Start',
    },
    {
      route: '/survey',
      caption: 'Take Survey',
    },
    {
      route: '/result',
      caption: 'See Result',
    }
  ]

  constructor() {}

  public menu(): MenuItem[] {
    return this.baseMenu;
  }
}
