import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from './material.module';
import { HeaderComponent } from './header/header.component';
import { SidenavListComponent } from './sidenav-list/sidenav-list.component';
import { MenuService } from './menu.service';

@NgModule({
  declarations: [
    SidenavListComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    MaterialModule,
    HeaderComponent,
    SidenavListComponent
  ],
  providers: [
    MenuService
  ]
})
export class LayoutModule { }
